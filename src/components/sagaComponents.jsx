import { useState, useRef, useCallback } from "react";
import { useBookSearch } from "./useBookSearch";

const SagaComponents = () => {
  const [query, setQuery] = useState("");

  const [pageNumber, setPageNumber] = useState(1);

  const handleSearch = (e) => {
    setQuery(e.target.value);
    setPageNumber(1);
  };

  const { loading, error, books, hasMore } = useBookSearch(query, pageNumber);
  console.log(books);

  const observer = useRef();
  const lastBookElementRef = useCallback(
    (node) => {
      if (loading) return;
      if (observer.current) observer.current.disconnect();
      observer.current = new IntersectionObserver((entries) => {
        if (entries[0].isIntersecting && hasMore) {
          setPageNumber((prevNumber) => prevNumber + 1);
        }
      });
      if (node) observer.current.observe(node);
      console.log(node);
    },
    [loading, hasMore]
  );

  return (
    <div>
      <input type="text" onChange={handleSearch} value={query} />
      {books.map((item, index) => {
        if (books.length === index + 1) {
          return (
            <div ref={lastBookElementRef} key={index}>
              {item}
            </div>
          );
        }
        return (
          <div key={index}>
            <p>{item}</p>
          </div>
        );
      })}
      <div>{loading && "Loading..."}</div>
      <div>{error && "error!"}</div>
    </div>
  );
};

export default SagaComponents;
