import "./App.css";
import SagaComponents from "./components/sagaComponents";

function App() {
  return (
    <div className="App">
      <SagaComponents />
    </div>
  );
}

export default App;
